# Build
FROM node:latest AS builder

RUN git clone https://github.com/gridsome/gridsome.org.git /gridsome.org

WORKDIR /gridsome.org

RUN npm install
RUN npm run build

# Serve
FROM nginx:alpine

COPY --from=builder /gridsome.org/dist /usr/share/nginx/html
